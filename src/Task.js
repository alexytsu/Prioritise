import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    Button,
    DatePickerAndroid,
    FlatList,
    StyleSheet,
    Text,
    TextInput,
    TouchableNativeFeedback,
    TouchableOpacity,
    View,
} from 'react-native';
import renderIf from './RenderIf.js';

class TaskCardBottom extends Component {
    render() {
        return (
            <View style={styles.taskBottom}>
                <View style={{ flex: 1, flexDirection:"row", justifyContent:"space-between" }}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.priority}> Priority: </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{textAlign:"right"}}>{this.props.priority}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

class TaskCardBar extends Component {
    render() {
        return (
            <View style={styles.task}>
                <View style={styles.dateholder}>
                    <Text onPress={this.props.dateAction} style={styles.date}> {this.props.date} </Text>
                </View>
                <TouchableOpacity style={styles.titleHolder}>
                    <Text
                        style={styles.barTitle}
                        onLongPress={this.props.titleAction}
                        onPress={this.props.editTitle}
                    >
                        {this.props.title}
                    </Text>
                </TouchableOpacity>
                <View style={styles.buttonholder}>
                    <TouchableNativeFeedback>
                        <View style={styles.checkButton1}></View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback>
                        <View style={styles.checkButton2}></View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback>
                        <View style={styles.checkButton3}></View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );
    }

}

class TaskCardTitleEditor extends Component {
    render() {
        return (
            <TextInput
                autoFocus={true}
                style={styles.titleInput}
                onSubmitEditing={this.props.editTitle}
                onEndEditing={this.props.editTitle}
                onChangeText={this.props.updateTitle}
                onFocus={this.props.toggleDrawer}
            >
                <Text style={styles.bottomTitle}>{this.props.title}</Text>
            </TextInput>
        );
    }
}

class TaskCard extends Component {

    constructor(props) {
        super(props);

        // Bind this
        this._toggleDrawer = this._toggleDrawer.bind(this);
        this._showPicker = this._showPicker.bind(this);
        this._editTitle = this._editTitle.bind(this);
        this._updateTitle = this._updateTitle.bind(this);

        // Create empty fields
        this.state = {
            title: props.title,
            description: 'Empty desc',
            showBottom: false,
            date: props.date,
        }
    }

    _editTitle() {
        this.setState({
            showTitleEditor: !this.state.showTitleEditor,
        });
    }

    _toggleDrawer() {
        this.setState({
            showBottom: !this.state.showBottom,
        });
    }

    async _showPicker() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                // Use `new Date()` for current date.
                // May 25 2020. Month 0 is January.
                date: new Date()
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                var fixedMonth = month + 1;
                this.setState({ date: day + '/' + fixedMonth + '/' + year % 100 });
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    }

    _alert() {
        Alert.alert("hi");
    }

    _updateTitle(text) {
        this.setState(
            { title: text }
        );
    }

    render() {
        return (
            <View styles={{ flex: 1 }}>
                <TaskCardBar
                    dateAction={this._showPicker}
                    titleAction={this._toggleDrawer}
                    title={this.state.title}
                    date={this.state.date}
                    editTitle={this._editTitle}
                />
                {renderIf(this.state.showTitleEditor,
                    <TaskCardTitleEditor
                        title={this.state.title}
                        editTitle={this._editTitle}
                        updateTitle={this._updateTitle}
                    />
                )}
                {renderIf(this.state.showBottom,
                    <TaskCardBottom
                        description={this.state.description}
                        title={this.state.title}
                        priority={this.props.priority}
                    />
                )}

            </View>

        );
    }
}

const styles = StyleSheet.create({
    task: {
        height: 50,
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 5,
        borderRadius: 3,
        marginHorizontal: 5,
        paddingLeft: 10,

    },
    taskBottom: {
        flex: 1,
        height: 100,
        backgroundColor: "#FFFFFF",
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        marginTop: -5,
        marginHorizontal: 5,
        paddingHorizontal: 10,

    },
    dateholder: {
        width: 80,
    },
    checkButton1: {
        flex: 1,
        backgroundColor: "dodgerblue",
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    checkButton2: {
        flex: 1,
        backgroundColor: "gold",
    },
    checkButton3: {
        flex: 1,
        backgroundColor: "seagreen",
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    date: {
        fontWeight: "bold",
    },
    titleHolder: {
        flex: 1,
        marginRight: 10,
    },
    titleBox: {
        height: 30
    },
    buttonholder: {
        width: 25 + "%",
        height: 85 + "%",
        paddingVertical: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        flexDirection: "row"
    },
    bottomTitle: {
        fontWeight: "bold",
    },
    titleInput: {
        backgroundColor: "#FFFFFF",
        flex: 1,
        height: 50,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        marginTop: -5,
        marginHorizontal: 5,
        paddingLeft: 10,
    },
    barTitle: {
        fontWeight: "bold",
        color: "black",
    },
    priority: {
        fontWeight: "bold",
    }
});

export default TaskCard;