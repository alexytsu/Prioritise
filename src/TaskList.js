import React, { Component } from 'react';
import {
    Alert,
    TouchableNativeFeedback,
    FlatList,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import TaskCard from './Task.js';

class TaskList extends Component {

    constructor(props) {
        super(props);
    }



    render(){
        return (
            <View style={styles.TaskList}>
                <FlatList
                    data={this.props.tasks}
                    renderItem={({ item }) => (
                        <TaskCard title={item.key} priority={item.priority} date={item.date} />
                    )}
                    removeClippedSubviews={false}
                    extraData={this.props}
                    
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    TaskList: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-between",
        backgroundColor: "#e1e8ee",
    },
});

export default TaskList;
