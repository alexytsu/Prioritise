import React, { Component } from 'react';
import { AsyncStorage, Alert, Platform, StyleSheet, Text, View, TouchableNativeFeedback } from 'react-native';
import { SearchBar, Overlay } from 'react-native-elements';
import TaskList from "./src/TaskList.js";
import TaskCard from "./src/Task.js";

export default class App extends Component {

  constructor(props) {
    super(props);

    this._addTask = this._addTask.bind(this);

    this.state = {
      isVisible: true,
      tasks: [
        {
          key: "Default Task",
          priority: "0",
          date: "10/12/18",
        }
      ],
    };
  }

  async storeList() {
    try {
      await AsyncStorage.setItem('@tasklist:key', "6");
    } catch (error) {
      Alert.alert("Fail");
    }
  }

  _addTask() {
    var placeHolderTask = {
      key: "Placeholder",
      priority: "10",
      date: "10/12/18",
    }
    var tasksBefore = this.state.tasks;
    var number = Math.floor(Math.random()*10);
    tasksBefore.push( placeHolderTask );
    tasksBefore.sort((a,b) => a.priority - b.priority);
    this.setState({ tasks: tasksBefore });
    this.storeList();
  }

  render() {
    return (
      <View style={styles.container}>

        <SearchBar
          showLoading
          lightTheme
          platform="android"
          placeholder='Search' />
        <TaskList
          tasks={this.state.tasks}
        />
        <View style={{ height: 60, backgroundColor: "white" }}>
          <View style={{ flex: 1, alignItems: "center" }}>
            <TouchableNativeFeedback onPress={this._addTask}>
              <View style={styles.AddTaskButton}>
                <Text style={styles.AddTaskText}>+ Add Task</Text>
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: '#EEEEEE',
  },
  AddTaskButton: {
    backgroundColor: 'steelblue',
    width: 30 + "%",
    padding: 10,
    borderRadius: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  AddTaskText: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
    color: "white",
  }
});
